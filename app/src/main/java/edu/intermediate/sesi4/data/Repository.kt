package edu.intermediate.sesi4.data

class Repository {

    fun getNewsList(): List<News> {
        return arrayListOf(
            News(
                "1",
                "theguardian.com",
                "What makes Elon Musk tick? I spent months following the same people as him to find out who fuels his curious worldview",
                "What’s it like to be Elon Musk? On almost every level it is impossible to imagine – he’s just too much. Musk is the hands-on head of three mega-companies, one (Tesla) wildly successful, one (SpaceX) madly aspirational, one (Twitter/X) a shambles",
                "https://c.biztoc.com/p/9f2569ecf8594466/s.webp",
                "2023-09-23T06:14:10Z"
            ),
            News(
                "2",
                "Onlineuserr",
                "Sign up, Deposit $5000 & Receive 16 Free Shares (from A$5.2 ea to A$426 ea) @ Moomoo",
                "As the promotion details have changed, here is a new opportunity to make some free money.\n\nPromotion available available only to new clients of Moomoo AU who haven't made a deposit",
                "https://files.ozbargain.com.au/n/49/801549.jpg?h=1f90c1e0",
                "2023-09-23T06:35:04Z"
            ),
            News(
                "3",
                "Hannah Getahun",
                "Elon Musk's grandfather belonged to a political party that believed the world should be governed by technology. Newspapers at the time described it as having 'the tone of an incipient Fascist movement.'",
                "Elon Musk's grandfather Joshua Haldeman was a raging racist and anti-semite, per the Atlantic. He also strongly believed in technocracy at one point in his life.",
                "https://i.insider.com/650e6be923ce9d001908aaa5?width=1200&format=jpeg",
                "2023-09-23T07:04:10Z"
            ),
            News(
                "4",
                "Kasim Kashgar",
                "Uyghur News Recap: Sept. 15– 22, 2023",
                "Hungary Quietly Hosts Xinjiang Official Sanctioned by the US for Alleged Human Rights Abuses\n\nHungary's discreet hosting of Erkin Tuniyaz, a U.S. sanctioned Xinjiang official accused of human rights abuses, aligns with China's soft power strategy to whitewash…",
                "https://gdb.voanews.com/01000000-0aff-0242-9299-08dbab4a59bf_cx0_cy8_cw0_w1200_r1.jpg",
                "2023-09-23T07:12:17Z"
            ),
            News(
                "5",
                "NDTV News",
                "YouTuber Criticised For Sneaking Into Bengaluru Metro Without Ticket",
                "YouTuber Fidias Panayiotou, who rose to fame for hugging billionaire Elon Musk earlier this year, is facing severe criticism online for showcasing how to enter Bengaluru Metro without a ticket.",
                "https://c.ndtvimg.com/2023-09/hcaml3h8_youtuber-fidias-panayiotou_625x300_23_September_23.jpg",
                "2023-09-23T07:16:51Z"
            )
        )
    }

    fun getNews(id: String): News {
        return getNewsList().find {
            it.id == id
        } ?: News()
    }
}