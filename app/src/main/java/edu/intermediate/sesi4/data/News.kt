package edu.intermediate.sesi4.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class News(
    val id: String = "",
    val author: String = "",
    val title: String = "",
    val desc: String = "",
    val imageUrl: String = "",
    val publishedDate: String = ""
) : Parcelable