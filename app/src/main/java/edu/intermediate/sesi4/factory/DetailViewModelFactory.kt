package edu.intermediate.sesi4.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import edu.intermediate.sesi4.data.Repository
import edu.intermediate.sesi4.presentation.detail.DetailViewModel

class DetailViewModelFactory(private val repository: Repository) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DetailViewModel(repository) as T
    }
}