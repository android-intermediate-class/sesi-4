package edu.intermediate.sesi4.presentation.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import edu.intermediate.sesi4.R
import edu.intermediate.sesi4.data.Repository
import edu.intermediate.sesi4.databinding.ActivityDetailBinding
import edu.intermediate.sesi4.factory.DetailViewModelFactory
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class DetailActivity : AppCompatActivity() {

    private var _layout: ActivityDetailBinding? = null

    private val layout: ActivityDetailBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    private lateinit var mViewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityDetailBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        mViewModel = ViewModelProvider(
            this,
            DetailViewModelFactory(Repository())
        )[DetailViewModel::class.java]

        val newsId = intent?.extras?.getString(NEWS_ID, "")

        mViewModel.newsLiveData.observe(this) { news ->
            val inputDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val outputDateFormat = SimpleDateFormat("dd MMM yyyy HH:mm", Locale("id", "ID"))
            val date = inputDateFormat.parse(news.publishedDate) ?: Date()

            Picasso.get()
                .load(news.imageUrl)
                .error(R.drawable.broken_image)
                .into(layout.ivNews)
            layout.tvTitle.text = news.title
            layout.tvDate.text = outputDateFormat.format(date)
            layout.tvAuthor.text = news.author
            layout.tvDesc.text = news.desc
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }

    companion object {
        const val NEWS_ID = "news_id"
    }
}