package edu.intermediate.sesi4.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import edu.intermediate.sesi4.data.News
import edu.intermediate.sesi4.data.Repository

class DetailViewModel(private val repository: Repository) : ViewModel() {

    private var _newsLiveData = MutableLiveData<News>()
    val newsLiveData: LiveData<News>
        get() = _newsLiveData

    fun getNews(id: String) {
        _newsLiveData.value = repository.getNews(id)
    }
}