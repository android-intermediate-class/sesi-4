package edu.intermediate.sesi4.presentation

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.intermediate.sesi4.data.Repository
import edu.intermediate.sesi4.databinding.ActivityMainBinding
import edu.intermediate.sesi4.factory.MainViewModelFactory
import edu.intermediate.sesi4.presentation.detail.DetailActivity

class MainActivity : AppCompatActivity() {

    private var _layout: ActivityMainBinding? = null
    private val layout: ActivityMainBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    private lateinit var mViewModel: MainViewModel
    private var mAdapter: NewsAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        mViewModel = ViewModelProvider(
            this,
            MainViewModelFactory(Repository())
        )[MainViewModel::class.java]

        mViewModel.newsListLiveData.observe(this) { news ->
            mAdapter?.setData(news)
        }

        mAdapter = NewsAdapter(mutableListOf()) { newsId ->
            val intent = Intent(this, DetailActivity::class.java).apply {
                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                putExtra(DetailActivity.NEWS_ID, newsId)
            }
            startActivity(intent)
        }

        layout.root.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = mAdapter
        }

        mViewModel.getNewsList()
    }

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }
}
