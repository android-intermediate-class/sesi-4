package edu.intermediate.sesi4.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import edu.intermediate.sesi4.data.News
import edu.intermediate.sesi4.data.Repository

class MainViewModel(private val repository: Repository) : ViewModel() {

    private var _newsListLiveData = MutableLiveData<List<News>>()
    val newsListLiveData: LiveData<List<News>>
        get() = _newsListLiveData

    fun getNewsList() {
        _newsListLiveData.value = repository.getNewsList()
    }
}