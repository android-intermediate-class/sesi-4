package edu.intermediate.sesi4.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import edu.intermediate.sesi4.R
import edu.intermediate.sesi4.data.News
import edu.intermediate.sesi4.databinding.ItemNewsBinding
import java.text.SimpleDateFormat
import java.util.Locale

class NewsAdapter(
    private val mNews: MutableList<News>,
    private val itemClick: (String) -> Unit
) : RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val binding = ItemNewsBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return NewsViewHolder(binding)
    }

    fun setData(news: List<News>) {
        mNews.clear()
        mNews.addAll(news)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.bind(mNews[position])
    }

    override fun getItemCount(): Int = mNews.size

    inner class NewsViewHolder(
        val binding: ItemNewsBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(news: News) {
            binding.root.setOnClickListener { itemClick(news.id) }
            val inputDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
            val outputDateFormat = SimpleDateFormat("dd MMM yyyy", Locale("id", "ID"))
            val date = inputDateFormat.parse(news.publishedDate)

            Picasso.get()
                .load(news.imageUrl)
                .error(R.drawable.broken_image)
                .into(binding.ivItemNews)
            binding.tvItemNewsTitle.text = news.title
            binding.tvItemNewsDate.text = outputDateFormat.format(date)
        }
    }
}